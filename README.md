# Динамический Веб контент
Vagrant автоматически поднимает машину master, ip 192.168.11.151

1. Устанавливается nginx
2. Настраиваются server block для трех инстансов
```
2.1 wordpress
2.2 grafana
2.3 ghost
```

3. Устанавливаются wordpress grafana и ghost
```
3.1 wordpress 192.168.11.151:8081
3.2 grafana   192.168.11.151:8082
3.3 ghost     192.168.11.151:8083
```

4. для красоты можно внести изменения в /etc/hosts на хостовой машине 
```
192.168.11.151 www.php.com php.com www.go.com go.com www.js.com js.com 
```
5. И с хоста ходить уже по доменным именам `php.com:8081`, `go.com:8082`, `js.com:8083`


